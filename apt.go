package goapt

import (
	"context"
	"io"
	"os/exec"
	"syscall"
)

// Stdout is a variable holding the writer for the output. If != nil, the std(out|err) for all commands is written here.
// For those where the output is parsed, the output will be sent after the execution using one Write call
var Stdout io.Writer

func Update(ctx context.Context) error {
	cmd := exec.CommandContext(ctx, "apt-get", "update", "-q")
	cmd.Stdout = Stdout
	cmd.Stderr = Stdout
	cmd.Env = append(syscall.Environ(), "DEBIAN_FRONTEND=noninteractive")
	err := cmd.Run()
	if err != nil {
		return err
	}
	return nil
}

func Upgrade(ctx context.Context, p ...Package) error {
	var args = []string{"upgrade", "-qy"}
	if len(p) > 0 {
		args = []string{"install", "-qy", "--only-upgrade"}
		for _, pkg := range p {
			args = append(args, pkg.Name+"="+pkg.NewVersion)
		}
	}
	cmd := exec.CommandContext(ctx, "apt-get", args...)
	cmd.Stdout = Stdout
	cmd.Stderr = Stdout
	cmd.Env = append(syscall.Environ(), "DEBIAN_FRONTEND=noninteractive")
	return cmd.Run()
}

func DistUpgrade(ctx context.Context, p ...Package) error {
	args := []string{"dist-upgrade", "-qy"}
	if len(p) > 0 {
		args = []string{"install", "-qy", "--only-upgrade"}
		for _, pkg := range p {
			args = append(args, pkg.Name+"="+pkg.NewVersion)
		}
	}
	cmd := exec.CommandContext(ctx, "apt-get", args...)
	cmd.Stdout = Stdout
	cmd.Stderr = Stdout
	cmd.Env = append(syscall.Environ(), "DEBIAN_FRONTEND=noninteractive")
	return cmd.Run()
}

func Install(ctx context.Context, p ...Package) error {
	args := []string{"install", "-qy"}
	for _, pkg := range p {
		var name = pkg.Name
		if pkg.NewVersion != "" {
			name += "=" + pkg.NewVersion
		}
		args = append(args, name)
	}
	cmd := exec.CommandContext(ctx, "apt-get", args...)
	cmd.Stdout = Stdout
	cmd.Stderr = Stdout
	cmd.Env = append(syscall.Environ(), "DEBIAN_FRONTEND=noninteractive")
	return cmd.Run()
}

func Remove(ctx context.Context, p ...Package) error {
	args := []string{"remove", "-qy"}
	for _, pkg := range p {
		args = append(args, pkg.Name)
	}
	cmd := exec.CommandContext(ctx, "apt-get", args...)
	cmd.Stdout = Stdout
	cmd.Stderr = Stdout
	cmd.Env = append(syscall.Environ(), "DEBIAN_FRONTEND=noninteractive")
	return cmd.Run()
}
