//nolint
package main

import (
	"fmt"
	goapt "gitlab.com/Enrico204/go-apt"
)

func main() {
	repos, err := goapt.ListRepositories()
	if err != nil {
		panic(err)
	}

	for _, r := range repos {
		fmt.Println(r)
	}
}
