package goapt

import (
	"bytes"
	"context"
	"fmt"
	"os/exec"
	"syscall"
)

func ListPackages(ctx context.Context) ([]Package, error) {
	cmd := exec.CommandContext(ctx, "dpkg", "-l")
	cmd.Env = append(syscall.Environ(), "DEBIAN_FRONTEND=noninteractive")
	output, err := cmd.Output()
	if err != nil {
		return nil, fmt.Errorf("dpkg -l: %w", err)
	}
	if Stdout != nil {
		_, _ = Stdout.Write(output)
	}
	return parseDpkgListOutput(bytes.NewReader(output))
}
