package goapt

import (
	"bytes"
	"context"
	"fmt"
	"os/exec"
	"syscall"
)

func ListUpgradable(ctx context.Context) (install []Package, upgrade []Package, remove []Package, err error) {
	cmd := exec.CommandContext(ctx, "apt-get", "--just-print", "upgrade", "-q")
	cmd.Env = append(syscall.Environ(), "DEBIAN_FRONTEND=noninteractive")
	output, err := cmd.Output()
	if err != nil {
		return install, upgrade, remove, fmt.Errorf("apt-get --just-print upgrade: %w", err)
	}
	if Stdout != nil {
		_, _ = Stdout.Write(output)
	}
	return parseAptGetPrintOutput(bytes.NewReader(output))
}

func ListDistUpgradable(ctx context.Context) (install []Package, upgrade []Package, remove []Package, err error) {
	cmd := exec.CommandContext(ctx, "apt-get", "--just-print", "dist-upgrade", "-q")
	cmd.Env = append(syscall.Environ(), "DEBIAN_FRONTEND=noninteractive")
	output, err := cmd.Output()
	if err != nil {
		return install, upgrade, remove, fmt.Errorf("apt-get --just-print dist-upgrade: %w", err)
	}
	if Stdout != nil {
		_, _ = Stdout.Write(output)
	}
	return parseAptGetPrintOutput(bytes.NewReader(output))
}
