package goapt

import "fmt"

type AptAction string

const (
	ActionInstall AptAction = "install"
	ActionUpgrade AptAction = "upgrade"
	ActionRemove  AptAction = "remove"
	ActionPurge   AptAction = "purge"
)

type Package struct {
	Name             string   `json:"name"`
	Version          string   `json:"version"`
	NewVersion       string   `json:"new_version"`
	Repositories     []string `json:"repositories"`
	Architecture     string   `json:"architecture"`
	Description      string   `json:"description"`
	DpkgStatus       string   `json:"dpkg_status"`
	UpgradeDependsOn []string `json:"upgrade_depends_on"`
	Purge            bool     `json:"purge"`
}

func (p Package) String() string {
	var x = "X"
	if p.Purge {
		x = "P"
	}
	if p.Version == "" {
		// New package
		return fmt.Sprint(p.Name, "/", p.Architecture, " ", p.Repositories, " ", x, " --> ", p.NewVersion)
	} else if p.NewVersion == "" {
		// Remove package
		return fmt.Sprint(p.Name, "/", p.Architecture, " ", p.Repositories, " ", p.Version, " --> ", x)
	}
	return fmt.Sprint(p.Name, "/", p.Architecture, " ", p.Repositories, " ", p.Version, " --> ", p.NewVersion)
}

func (p Package) Equals(q Package) bool {
	if !(p.Name == q.Name &&
		p.Version == q.Version &&
		p.NewVersion == q.NewVersion &&
		p.Purge == q.Purge &&
		len(p.Repositories) == len(q.Repositories) &&
		p.Architecture == q.Architecture) {
		return false
	}

	for _, r1 := range p.Repositories {
		var found = false
		for _, r2 := range q.Repositories {
			if r1 == r2 {
				found = true
				break
			}
		}
		if !found {
			return false
		}
	}
	return true
}

func (p Package) Action() AptAction {
	if p.Purge {
		return ActionPurge
	} else if p.Version == "" {
		return ActionInstall
	} else if p.NewVersion == "" {
		return ActionRemove
	}
	return ActionUpgrade
}
