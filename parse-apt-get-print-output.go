package goapt

import (
	"bufio"
	"fmt"
	"io"
	"regexp"
	"strings"
)

var (
	// Inst xxd (2:8.2.2434-3+deb11u1 Debian:11.3/stable [amd64])
	installRE = regexp.MustCompile(`^Inst ([^ ]+) \(([^ ]+) (.*?) \[([^ ]+)]\)`)

	// Inst base-files [11.1+deb11u2] (11.1+deb11u3 Debian:11.3/stable [amd64])
	upgradeRE = regexp.MustCompile(`^Inst ([^ ]+) \[([^ ]+)] \(([^ ]+) (.*?) \[([^ ]+)]\)`)

	// Remv vim [2:8.2.2434-3+deb11u1]
	removeRE = regexp.MustCompile(`^Remv ([^ ]+) \[([^ ]+)]`)

	// Purg vim [2:8.2.2434-3+deb11u1]
	purgeRE = regexp.MustCompile(`^Purg ([^ ]+) \[([^ ]+)]`)

	// Lines starting with this prefix should be parsed
	outputLineRE = regexp.MustCompile("^(Remv|Inst)")

	// TODO: Inst gpg-wks-client [2.2.27-2+deb11u1] (2.2.27-2+deb11u2 Debian-Security:11/stable-security [amd64]) [gnupg:amd64 ]
)

// parseAptGetPrintOutput parses the apt-get output when invoked with --just-print
func parseAptGetPrintOutput(output io.Reader) (install []Package, upgrade []Package, remove []Package, err error) {
	scanner := bufio.NewScanner(output)
	for scanner.Scan() {
		var line = scanner.Text()
		switch {
		case installRE.MatchString(line):
			var str = installRE.FindAllStringSubmatch(line, -1)
			install = append(install, Package{
				Name:         str[0][1],
				NewVersion:   str[0][2],
				Repositories: strings.Split(str[0][3], ", "),
				Architecture: str[0][4],
			})
		case upgradeRE.MatchString(line):
			var str = upgradeRE.FindAllStringSubmatch(line, -1)
			upgrade = append(upgrade, Package{
				Name:         str[0][1],
				Version:      str[0][2],
				NewVersion:   str[0][3],
				Repositories: strings.Split(str[0][4], ", "),
				Architecture: str[0][5],
			})
		case removeRE.MatchString(line):
			var str = removeRE.FindAllStringSubmatch(line, -1)
			remove = append(remove, Package{
				Name:    str[0][1],
				Version: str[0][2],
			})
		case purgeRE.MatchString(line):
			var str = purgeRE.FindAllStringSubmatch(line, -1)
			remove = append(remove, Package{
				Name:    str[0][1],
				Version: str[0][2],
				Purge:   true,
			})
		case outputLineRE.MatchString(line):
			return install, upgrade, remove, fmt.Errorf("error parsing output line: %s", line)
		default:
			// Skip other lines, such as the description of actions
		}
	}
	return install, upgrade, remove, nil
}
