package goapt

import (
	"strings"
	"testing"
)

func TestParseAptGetPrintOutput(t *testing.T) {
	t.Run("upgrade-only", func(t *testing.T) {
		install, upgrade, remove, err := parseAptGetPrintOutput(strings.NewReader(upgradeOnlyOutput))
		if err != nil {
			t.Fatal(err)
		}
		if len(remove) > 0 {
			t.Fatal("No removal expected, parsed: ", remove)
		}
		if len(install) > 0 {
			t.Fatal("No install expected, parsed: ", install)
		}
		if len(upgrade) != 13 {
			t.Fatal("Expected 13 packages to upgrade, found: ", len(upgrade))
		}

		var tpkg = Package{
			Name:         "dpkg",
			Version:      "1.20.9",
			NewVersion:   "1.20.10",
			Repositories: []string{"Debian-Security:11/stable-security"},
			Architecture: "amd64",
		}
		if !tpkg.Equals(upgrade[1]) {
			t.Fatal("Expected ", tpkg, ", found: ", upgrade[1])
		}
	})

	t.Run("install-only", func(t *testing.T) {
		install, upgrade, remove, err := parseAptGetPrintOutput(strings.NewReader(installOnlyOutput))
		if err != nil {
			t.Fatal(err)
		}
		if len(remove) > 0 {
			t.Fatal("No removal expected, parsed: ", remove)
		}
		if len(upgrade) > 0 {
			t.Fatal("No upgrade expected, parsed: ", upgrade)
		}
		if len(install) != 5 {
			t.Fatal("Expected 5 packages to install, found: ", len(install))
		}
		var tpkg = Package{
			Name:         "vim-common",
			NewVersion:   "2:8.2.2434-3+deb11u1",
			Repositories: []string{"Debian:11.3/stable"},
			Architecture: "all",
		}
		if !tpkg.Equals(install[1]) {
			t.Fatal("Expected ", tpkg, ", found: ", install[1])
		}
	})

	t.Run("install-and-upgrade", func(t *testing.T) {
		install, upgrade, remove, err := parseAptGetPrintOutput(strings.NewReader(installAndUpgrade))
		if err != nil {
			t.Fatal(err)
		}
		if len(remove) > 0 {
			t.Fatal("No removal expected, parsed: ", remove)
		}
		if len(upgrade) > 1 {
			t.Fatal("Expected 1 package to upgrade, found: ", len(upgrade))
		}
		if len(install) != 24 {
			t.Log(install)
			t.Fatal("Expected 24 packages to install, found: ", len(install))
		}
		var tpkg = Package{
			Name:         "gpgv",
			Version:      "2.2.27-2",
			NewVersion:   "2.2.27-2+deb11u1",
			Repositories: []string{"Debian:11.3/stable"},
			Architecture: "amd64",
		}
		if !tpkg.Equals(upgrade[0]) {
			t.Fatal("Expected ", tpkg, ", found: ", upgrade[0])
		}

		tpkg = Package{
			Name:         "readline-common",
			NewVersion:   "8.1-1",
			Repositories: []string{"Debian:11.3/stable"},
			Architecture: "all",
		}
		if !tpkg.Equals(install[0]) {
			t.Fatal("Expected ", tpkg, ", found: ", install[0])
		}

		// Multi-repo
		tpkg = Package{
			Name:         "libsasl2-modules-db",
			NewVersion:   "2.1.27+dfsg-2.1+deb11u1",
			Repositories: []string{"Debian:11.3/stable", "Debian-Security:11/stable-security"},
			Architecture: "amd64",
		}
		if !tpkg.Equals(install[5]) {
			t.Fatal("Expected ", tpkg, ", found: ", install[5])
		}
	})

	t.Run("remove-only", func(t *testing.T) {
		install, upgrade, remove, err := parseAptGetPrintOutput(strings.NewReader(removeOnly))
		if err != nil {
			t.Fatal(err)
		}
		if len(install) > 0 {
			t.Fatal("No install expected, parsed: ", install)
		}
		if len(upgrade) > 0 {
			t.Fatal("No upgrade expected, parsed: ", upgrade)
		}
		if len(remove) != 1 {
			t.Fatal("Expected 1 package to remove, found: ", len(remove))
		}
		var tpkg = Package{
			Name:    "vim-tiny",
			Version: "2:8.2.2434-3+deb11u1",
		}
		if !tpkg.Equals(remove[0]) {
			t.Fatal("Expected ", tpkg, ", found: ", remove[0])
		}
	})
}

const (
	installOnlyOutput = `
Reading package lists...
Building dependency tree...
Reading state information...
The following additional packages will be installed:
  libgpm2 vim-common vim-runtime xxd
Suggested packages:
  gpm ctags vim-doc vim-scripts
The following NEW packages will be installed:
  libgpm2 vim vim-common vim-runtime xxd
0 upgraded, 5 newly installed, 0 to remove and 13 not upgraded.
Inst xxd (2:8.2.2434-3+deb11u1 Debian:11.3/stable [amd64])
Inst vim-common (2:8.2.2434-3+deb11u1 Debian:11.3/stable [all])
Inst libgpm2 (1.20.7-8 Debian:11.3/stable [amd64])
Inst vim-runtime (2:8.2.2434-3+deb11u1 Debian:11.3/stable [all])
Inst vim (2:8.2.2434-3+deb11u1 Debian:11.3/stable [amd64])
Conf xxd (2:8.2.2434-3+deb11u1 Debian:11.3/stable [amd64])
Conf vim-common (2:8.2.2434-3+deb11u1 Debian:11.3/stable [all])
Conf libgpm2 (1.20.7-8 Debian:11.3/stable [amd64])
Conf vim-runtime (2:8.2.2434-3+deb11u1 Debian:11.3/stable [all])
Conf vim (2:8.2.2434-3+deb11u1 Debian:11.3/stable [amd64])
`

	upgradeOnlyOutput = `
Reading package lists...
Building dependency tree...
Reading state information...
Calculating upgrade...
The following packages will be upgraded:
  base-files dpkg gpgv gzip libc-bin libc6 liblzma5 libssl1.1 libsystemd0 libudev1 sysvinit-utils tzdata zlib1g
13 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.
Inst base-files [11.1+deb11u2] (11.1+deb11u3 Debian:11.3/stable [amd64])
Conf base-files (11.1+deb11u3 Debian:11.3/stable [amd64])
Inst dpkg [1.20.9] (1.20.10 Debian-Security:11/stable-security [amd64])
Conf dpkg (1.20.10 Debian-Security:11/stable-security [amd64])
Inst gzip [1.10-4] (1.10-4+deb11u1 Debian-Security:11/stable-security [amd64])
Conf gzip (1.10-4+deb11u1 Debian-Security:11/stable-security [amd64])
Inst libc6 [2.31-13+deb11u2] (2.31-13+deb11u3 Debian:11.3/stable [amd64])
Conf libc6 (2.31-13+deb11u3 Debian:11.3/stable [amd64])
Inst libc-bin [2.31-13+deb11u2] (2.31-13+deb11u3 Debian:11.3/stable [amd64])
Conf libc-bin (2.31-13+deb11u3 Debian:11.3/stable [amd64])
Inst sysvinit-utils [2.96-7] (2.96-7+deb11u1 Debian:11.3/stable [amd64])
Conf sysvinit-utils (2.96-7+deb11u1 Debian:11.3/stable [amd64])
Inst libsystemd0 [247.3-6] (247.3-7 Debian:11.3/stable [amd64])
Conf libsystemd0 (247.3-7 Debian:11.3/stable [amd64])
Inst zlib1g [1:1.2.11.dfsg-2] (1:1.2.11.dfsg-2+deb11u1 Debian-Security:11/stable-security [amd64])
Conf zlib1g (1:1.2.11.dfsg-2+deb11u1 Debian-Security:11/stable-security [amd64])
Inst gpgv [2.2.27-2] (2.2.27-2+deb11u1 Debian:11.3/stable [amd64])
Conf gpgv (2.2.27-2+deb11u1 Debian:11.3/stable [amd64])
Inst liblzma5 [5.2.5-2] (5.2.5-2.1~deb11u1 Debian-Security:11/stable-security [amd64])
Conf liblzma5 (5.2.5-2.1~deb11u1 Debian-Security:11/stable-security [amd64])
Inst libssl1.1 [1.1.1k-1+deb11u2] (1.1.1n-0+deb11u3 Debian-Security:11/stable-security [amd64])
Conf libssl1.1 (1.1.1n-0+deb11u3 Debian-Security:11/stable-security [amd64])
Inst libudev1 [247.3-6] (247.3-7 Debian:11.3/stable [amd64])
Conf libudev1 (247.3-7 Debian:11.3/stable [amd64])
Inst tzdata [2021a-1+deb11u2] (2021a-1+deb11u4 Debian:11-updates/stable-updates [all])
Conf tzdata (2021a-1+deb11u4 Debian:11-updates/stable-updates [all])
`

	installAndUpgrade = `
Reading package lists...
Building dependency tree...
Reading state information...
The following additional packages will be installed:
  dirmngr gnupg gnupg-l10n gnupg-utils gpg-agent gpg-wks-client gpg-wks-server gpgconf gpgsm gpgv libassuan0 libgpm2 libksba8 libldap-2.4-2 libldap-common libncursesw6 libnpth0 libreadline8 libsasl2-2 libsasl2-modules libsasl2-modules-db libsqlite3-0 pinentry-curses
  readline-common
Suggested packages:
  dbus-user-session libpam-systemd pinentry-gnome3 tor parcimonie xloadimage scdaemon gpm libsasl2-modules-gssapi-mit | libsasl2-modules-gssapi-heimdal libsasl2-modules-ldap libsasl2-modules-otp libsasl2-modules-sql pinentry-doc readline-doc
The following NEW packages will be installed:
  dirmngr gnupg gnupg-l10n gnupg-utils gpg gpg-agent gpg-wks-client gpg-wks-server gpgconf gpgsm libassuan0 libgpm2 libksba8 libldap-2.4-2 libldap-common libncursesw6 libnpth0 libreadline8 libsasl2-2 libsasl2-modules libsasl2-modules-db libsqlite3-0 pinentry-curses
  readline-common
The following packages will be upgraded:
  gpgv
1 upgraded, 24 newly installed, 0 to remove and 12 not upgraded.
Inst gpgv [2.2.27-2] (2.2.27-2+deb11u1 Debian:11.3/stable [amd64])
Conf gpgv (2.2.27-2+deb11u1 Debian:11.3/stable [amd64])
Inst readline-common (8.1-1 Debian:11.3/stable [all])
Inst libreadline8 (8.1-1 Debian:11.3/stable [amd64])
Inst libassuan0 (2.5.3-7.1 Debian:11.3/stable [amd64])
Inst gpgconf (2.2.27-2+deb11u1 Debian:11.3/stable [amd64])
Inst libksba8 (1.5.0-3 Debian:11.3/stable [amd64])
Inst libsasl2-modules-db (2.1.27+dfsg-2.1+deb11u1 Debian:11.3/stable, Debian-Security:11/stable-security [amd64])
Inst libsasl2-2 (2.1.27+dfsg-2.1+deb11u1 Debian:11.3/stable, Debian-Security:11/stable-security [amd64])
Inst libldap-2.4-2 (2.4.57+dfsg-3+deb11u1 Debian-Security:11/stable-security [amd64])
Inst libnpth0 (1.6-3 Debian:11.3/stable [amd64])
Inst dirmngr (2.2.27-2+deb11u1 Debian:11.3/stable [amd64])
Inst gnupg-l10n (2.2.27-2+deb11u1 Debian:11.3/stable [all])
Inst gnupg-utils (2.2.27-2+deb11u1 Debian:11.3/stable [amd64])
Inst libsqlite3-0 (3.34.1-3 Debian:11.3/stable [amd64])
Inst gpg (2.2.27-2+deb11u1 Debian:11.3/stable [amd64])
Inst libncursesw6 (6.2+20201114-2 Debian:11.3/stable [amd64])
Inst pinentry-curses (1.1.0-4 Debian:11.3/stable [amd64])
Inst gpg-agent (2.2.27-2+deb11u1 Debian:11.3/stable [amd64])
Inst gpg-wks-client (2.2.27-2+deb11u1 Debian:11.3/stable [amd64])
Inst gpg-wks-server (2.2.27-2+deb11u1 Debian:11.3/stable [amd64])
Inst gpgsm (2.2.27-2+deb11u1 Debian:11.3/stable [amd64])
Inst gnupg (2.2.27-2+deb11u1 Debian:11.3/stable [all])
Inst libgpm2 (1.20.7-8 Debian:11.3/stable [amd64])
Inst libldap-common (2.4.57+dfsg-3+deb11u1 Debian-Security:11/stable-security [all])
Inst libsasl2-modules (2.1.27+dfsg-2.1+deb11u1 Debian:11.3/stable, Debian-Security:11/stable-security [amd64])
Conf readline-common (8.1-1 Debian:11.3/stable [all])
Conf libreadline8 (8.1-1 Debian:11.3/stable [amd64])
Conf libassuan0 (2.5.3-7.1 Debian:11.3/stable [amd64])
Conf gpgconf (2.2.27-2+deb11u1 Debian:11.3/stable [amd64])
Conf libksba8 (1.5.0-3 Debian:11.3/stable [amd64])
Conf libsasl2-modules-db (2.1.27+dfsg-2.1+deb11u1 Debian:11.3/stable, Debian-Security:11/stable-security [amd64])
Conf libsasl2-2 (2.1.27+dfsg-2.1+deb11u1 Debian:11.3/stable, Debian-Security:11/stable-security [amd64])
Conf libldap-2.4-2 (2.4.57+dfsg-3+deb11u1 Debian-Security:11/stable-security [amd64])
Conf libnpth0 (1.6-3 Debian:11.3/stable [amd64])
Conf dirmngr (2.2.27-2+deb11u1 Debian:11.3/stable [amd64])
Conf gnupg-l10n (2.2.27-2+deb11u1 Debian:11.3/stable [all])
Conf gnupg-utils (2.2.27-2+deb11u1 Debian:11.3/stable [amd64])
Conf libsqlite3-0 (3.34.1-3 Debian:11.3/stable [amd64])
Conf gpg (2.2.27-2+deb11u1 Debian:11.3/stable [amd64])
Conf libncursesw6 (6.2+20201114-2 Debian:11.3/stable [amd64])
Conf pinentry-curses (1.1.0-4 Debian:11.3/stable [amd64])
Conf gpg-agent (2.2.27-2+deb11u1 Debian:11.3/stable [amd64])
Conf gpg-wks-client (2.2.27-2+deb11u1 Debian:11.3/stable [amd64])
Conf gpg-wks-server (2.2.27-2+deb11u1 Debian:11.3/stable [amd64])
Conf gpgsm (2.2.27-2+deb11u1 Debian:11.3/stable [amd64])
Conf gnupg (2.2.27-2+deb11u1 Debian:11.3/stable [all])
Conf libgpm2 (1.20.7-8 Debian:11.3/stable [amd64])
Conf libldap-common (2.4.57+dfsg-3+deb11u1 Debian-Security:11/stable-security [all])
Conf libsasl2-modules (2.1.27+dfsg-2.1+deb11u1 Debian:11.3/stable, Debian-Security:11/stable-security [amd64])
`

	removeOnly = `
Reading package lists...
Building dependency tree...
Reading state information...
The following packages were automatically installed and are no longer required:
  vim-common xxd
Use 'apt autoremove' to remove them.
The following packages will be REMOVED:
  vim-tiny
0 upgraded, 0 newly installed, 1 to remove and 13 not upgraded.
Remv vim-tiny [2:8.2.2434-3+deb11u1]
`
)
