package goapt

import (
	"bufio"
	"fmt"
	"io"
	"regexp"
	"strings"
)

var (
	dpkgLineRE = regexp.MustCompile(`^([a-zA-Z]{2,3})\s+([^ ]+)\s+([^ ]+)\s+([^ ]+)\s*(.*?)$`)
)

func parseDpkgListOutput(output io.Reader) ([]Package, error) {
	var packages []Package
	var headerSkipped = false
	scanner := bufio.NewScanner(output)
	for scanner.Scan() {
		var line = scanner.Text()

		// Skip header
		if strings.HasPrefix(line, "+++-=") {
			// Header ends here
			headerSkipped = true
			continue
		} else if !headerSkipped {
			continue
		}

		if !dpkgLineRE.MatchString(line) {
			return nil, fmt.Errorf("error parsing line: %s", line)
		}
		str := dpkgLineRE.FindAllStringSubmatch(line, -1)

		packages = append(packages, Package{
			Name:         str[0][2],
			Version:      str[0][3],
			Architecture: str[0][4],
			Description:  str[0][5],
			DpkgStatus:   str[0][1],
		})
	}
	return packages, nil
}
