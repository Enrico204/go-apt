package goapt

import (
	"strings"
	"testing"
)

func TestParseDpkgListOutput(t *testing.T) {
	packages, err := parseDpkgListOutput(strings.NewReader(dpkgListOutput))
	if err != nil {
		t.Fatal(err)
	}

	if len(packages) != 5 {
		t.Fatal("Expected 5 packages, found: ", len(packages))
	}
	var tpkg = Package{
		Name:         "adduser",
		Version:      "3.118",
		Architecture: "all",
		Description:  "add and remove users and groups",
	}
	if !tpkg.Equals(packages[0]) {
		t.Fatal("Expected: ", tpkg, " found: ", packages[0])
	}
}

const (
	dpkgListOutput = `
Desired=Unknown/Install/Remove/Purge/Hold
| Status=Not/Inst/Conf-files/Unpacked/halF-conf/Half-inst/trig-aWait/Trig-pend
|/ Err?=(none)/Reinst-required (Status,Err: uppercase=bad)
||/ Name                    Version                      Architecture Description
+++-=======================-============================-============-========================================================================
ii  adduser                 3.118                        all          add and remove users and groups
ii  apt                     2.2.4                        amd64        commandline package manager
ii  base-files              11.1+deb11u3                 amd64        Debian base system miscellaneous files
ii  base-passwd             3.5.51                       amd64        Debian base system master password and group files
ii  bash                    5.1-2+b3                     amd64        GNU Bourne Again SHell
`
)
