package goapt

import (
	"bufio"
	"fmt"
	"io"
	"regexp"
	"strings"
)

var (
	repoLineRE = regexp.MustCompile(`^deb(-src)? (\[[^]]+] )?([^ ]+) ([^ ]+) ?(.*?)$`)
)

func parseSourceList(output io.Reader) ([]Repository, error) {
	var repos []Repository
	scanner := bufio.NewScanner(output)
	for scanner.Scan() {
		var line = scanner.Text()

		if strings.HasPrefix(line, "#") || strings.TrimSpace(line) == "" {
			// Skip comments and empty lines
			continue
		}

		if !repoLineRE.MatchString(line) {
			return nil, fmt.Errorf("error parsing line: %s", line)
		}
		str := repoLineRE.FindAllStringSubmatch(line, -1)

		var options []string
		if strings.TrimSpace(str[0][2]) != "" {
			options = strings.Split(strings.Trim(str[0][2], " []"), " ")
		}
		repos = append(repos, Repository{
			IsSource:     str[0][1] == "-src",
			Options:      options,
			URL:          str[0][3],
			Distribution: str[0][4],
			Components:   strings.Split(str[0][5], " "),
		})
	}
	return repos, nil
}
