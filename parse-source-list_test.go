package goapt

import (
	"strings"
	"testing"
)

func TestParseSourceList(t *testing.T) {
	repos, err := parseSourceList(strings.NewReader(aptRepoTestContent))
	if err != nil {
		t.Fatal(err)
	}

	if len(repos) != 4 {
		t.Fatal("Expected 4 repos, found: ", len(repos))
	}
	var r1 = Repository{
		IsSource:     false,
		URL:          "http://deb.debian.org/debian",
		Distribution: "bullseye",
		Components:   []string{"main"},
	}
	if !r1.Equals(repos[0]) {
		t.Fatal("Expected ", r1, " found ", repos[0])
	}

	var r2 = Repository{
		IsSource:     true,
		URL:          "http://security.debian.org/debian-security",
		Distribution: "bullseye-security",
		Components:   []string{"main"},
	}
	if !r2.Equals(repos[1]) {
		t.Fatal("Expected ", r2, " found ", repos[1])
	}

	var r3 = Repository{
		IsSource:     false,
		URL:          "http://deb.debian.org/debian",
		Distribution: "bullseye-updates",
		Components:   []string{"main", "contrib", "non-free"},
	}
	if !r3.Equals(repos[2]) {
		t.Fatal("Expected ", r3, " found ", repos[2])
	}

	var r4 = Repository{
		IsSource:     false,
		URL:          "http://apt.postgresql.org/pub/repos/apt",
		Distribution: "bullseye-pgdg",
		Components:   []string{"main"},
		Options:      []string{"arch=amd64"},
	}
	if !r4.Equals(repos[3]) {
		t.Fatal("Expected ", r4, " found ", repos[3])
	}

	for _, r := range repos {
		t.Log(r)
	}
}

const (
	aptRepoTestContent = `
# deb http://snapshot.debian.org/archive/debian/20220316T000000Z bullseye main
deb http://deb.debian.org/debian bullseye main

# deb http://snapshot.debian.org/archive/debian-security/20220316T000000Z bullseye-security main
deb-src http://security.debian.org/debian-security bullseye-security main

# deb http://snapshot.debian.org/archive/debian/20220316T000000Z bullseye-updates main
deb http://deb.debian.org/debian bullseye-updates main contrib non-free

deb [arch=amd64] http://apt.postgresql.org/pub/repos/apt bullseye-pgdg main
`
)
