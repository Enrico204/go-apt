package goapt

import (
	"fmt"
	"os"
	"strings"
)

type Repository struct {
	IsSource     bool     `json:"is_source"`
	URL          string   `json:"url"`
	Distribution string   `json:"distribution"`
	Components   []string `json:"components"`
	Options      []string `json:"options"`
}

func (r Repository) String() string {
	var str = "deb"
	if r.IsSource {
		str = "deb-src"
	}
	if len(r.Options) > 0 {
		str += " [" + strings.Join(r.Options, " ") + "]"
	}
	return fmt.Sprintf("%s %s %s %s", str, r.URL, r.Distribution, strings.Join(r.Components, " "))
}

func (r Repository) Equals(q Repository) bool {
	if !(r.IsSource == q.IsSource &&
		r.URL == q.URL &&
		r.Distribution == q.Distribution &&
		len(r.Components) == len(q.Components) &&
		len(r.Options) == len(q.Options)) {
		return false
	}

	for _, r1 := range r.Components {
		var found = false
		for _, r2 := range q.Components {
			if r1 == r2 {
				found = true
				break
			}
		}
		if !found {
			return false
		}
	}
	for _, o1 := range r.Options {
		var found = false
		for _, o2 := range q.Options {
			if o1 == o2 {
				found = true
				break
			}
		}
		if !found {
			return false
		}
	}
	return true
}

// ListRepositories return the list of all repositories configured in apt
func ListRepositories() ([]Repository, error) {
	fp, err := os.Open("/etc/apt/sources.list")
	if err != nil {
		return nil, fmt.Errorf("opening /etc/apt/sources.list: %w", err)
	}

	repos, err := parseSourceList(fp)
	if err != nil {
		return nil, fmt.Errorf("parsing /etc/apt/sources.list: %w", err)
	}
	_ = fp.Close()

	items, err := os.ReadDir("/etc/apt/sources.list.d/")
	if err != nil {
		return nil, fmt.Errorf("reading /etc/apt/sources.list.d/: %w", err)
	}

	for _, item := range items {
		if !item.IsDir() && strings.HasSuffix(item.Name(), ".list") {
			fp, err := os.Open("/etc/apt/sources.list.d/" + item.Name())
			if err != nil {
				return nil, fmt.Errorf("reading /etc/apt/sources.list.d/%s: %w", item.Name(), err)
			}

			frepos, err := parseSourceList(fp)
			if err != nil {
				return nil, fmt.Errorf("parsing /etc/apt/sources.list.d/%s: %w", item.Name(), err)
			}
			_ = fp.Close()

			repos = append(repos, frepos...)
		}
	}
	return repos, nil
}
